$(document).ready(function() {
  // $(window).scroll(function() {
  //   if ($(window).scrollTop() > 0) {
  //     $(".sticky-header").addClass("fixed");
  //   } else {
  //     $(".sticky-header").removeClass("fixed");
  //   }
  // });

  function toggleNavbarMethod() {
    if ($(window).width() > 768) {
      $(".navbar .dropdown")
        .on("mouseover", function() {
          $(".dropdown-toggle", this).trigger("click");
        })
        .on("mouseout", function() {
          $(".dropdown-toggle", this)
            .trigger("click")
            .blur();
        });
    } else {
      $(".navbar .dropdown")
        .off("mouseover")
        .off("mouseout");
    }
  }
  toggleNavbarMethod();
  $(window).resize(toggleNavbarMethod);

  $('header .navbar .navbar-toggler').on('click', function(){
    $('body').addClass('body_fixed');
    if($('header .navbar .main-nav').hasClass('show')){
      $('body').removeClass('body_fixed');
    }

  });

  //////// search //////////////////////
  $('body').on('click',function() {
    $('.search_box').slideUp();
    $('body').removeClass('search_open');
});
$('.btn_search').click(function(e){
    $('.search_box').slideDown();
    $('body').addClass('search_open');
    //if($('.navbar-toggler').hasClass(''))
    //$('.navbar-toggler').addClass('collapsed');

    e.stopPropagation();
});
$('.search_box .btn-close').click(function(){
    $('.search_box').slideUp();
    $('body').removeClass('search_open');
});
$('.search_box').click(function(e){
    e.stopPropagation();
});


////////// back to top ////////////////
var btn = $('.back-to-top');
$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});
btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});
////////// @end back to top ////////////////


/* common tabs */
    $('.gallery-tabs .nav-link').bind('click', function(e) {
        e.preventDefault();
        var self2 = $(this);
        var scrollArea2 = $('.gallery-tabs');
        var leftOffset2 = self2.offset().left - scrollArea2.offset().left + scrollArea2.scrollLeft() - 40;

        scrollArea2.animate({ scrollLeft: leftOffset2 });

        });

});
