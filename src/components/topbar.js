import React from "react"

function TopBar(props) {
  return (
    <div class="top_bar">
      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light flex-wrap">
          <div class="collapse navbar-collapse
                  navbarSupportedContent">
            <ul class="navbar-nav ml-auto topnav">
              <li>
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="18.13" height="20.895" viewBox="0 0 18.13 20.895">
                    <g id="Group_900" data-name="Group 900" transform="translate(-1.337 0.5)">
                      <circle id="Ellipse_16" data-name="Ellipse 16" cx="2.082" cy="2.082" r="2.082" transform="translate(5.903 0)" fill="none" stroke="#024594" stroke-miterlimit="10" stroke-width="1"></circle>
                      <path id="Path_1863" data-name="Path 1863" d="M19.463,15.488a1.33,1.33,0,0,0-1.838,0l-.33.331L14.747,13.27a2.07,2.07,0,0,0-1.472-.609H10.634a.417.417,0,0,1-.416-.416V9.919l.954.954a.423.423,0,0,0,.346.119l2.457-.307a1.283,1.283,0,0,0,1.109-1.456,1.292,1.292,0,0,0-1.475-1.085l-1.283.208L10.438,6.465A1.593,1.593,0,0,0,7.719,7.589v5.487A2.084,2.084,0,0,0,9.8,15.158h3.3l3.381,3.381a1.25,1.25,0,0,0,1.767,0l1.213-1.214A1.3,1.3,0,0,0,19.463,15.488Z" transform="translate(-0.985 -1.004)" fill="none" stroke="#024594" stroke-miterlimit="10" stroke-width="1"></path>
                      <path id="Path_1864" data-name="Path 1864" d="M6.97,21.89a5.13,5.13,0,0,1-1.711-9.967.046.046,0,0,1,.017,0,.049.049,0,0,1,.015.1,5.032,5.032,0,1,0,6.554,5.973.05.05,0,0,1,.047-.037.105.105,0,0,1,.022,0,.051.051,0,0,1,.027.059A5.123,5.123,0,0,1,6.97,21.89Z" transform="translate(0 -1.995)" fill="none" stroke="#024594" stroke-miterlimit="10" stroke-width="1"></path>
                    </g>
                  </svg>
                           Accessibility
                        </a>
              </li>
              <li>
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19.181" height="18.913" viewBox="0 0 19.181 18.913">
                    <g id="Book_Icon" transform="translate(-67.5 -316.063)">
                      <g id="Group_885" data-name="Group 885" transform="translate(68 316.563)">
                        <path id="Path_1829" data-name="Path 1829" d="M74.158,333.076H68.586a.586.586,0,0,1-.586-.586v-12.9a.586.586,0,0,1,.586-.586H83.835a.586.586,0,0,1,.587.586v5.278" transform="translate(-68 -318.389)" fill="none" stroke="#024594" stroke-linecap="square" stroke-linejoin="round" stroke-width="1"></path>
                        <line id="Line_54" data-name="Line 54" y2="1" transform="translate(2.181)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></line>
                        <line id="Line_55" data-name="Line 55" y2="1" transform="translate(5.181)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></line>
                        <line id="Line_56" data-name="Line 56" y2="1" transform="translate(8.181)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></line>
                        <line id="Line_57" data-name="Line 57" y2="1" transform="translate(11.181)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></line>
                        <line id="Line_58" data-name="Line 58" y2="1" transform="translate(14.181)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></line>
                        <line id="Line_59" data-name="Line 59" x2="12" transform="translate(2.181 4)" fill="none" stroke="#024594" stroke-linecap="square" stroke-linejoin="round" stroke-width="1"></line>
                      </g>
                      <path id="Path_1830" data-name="Path 1830" d="M103.451,351.451v3.226h-3.226v-3.226H97v-3.226h3.226V345h3.226v3.226h3.226v3.226Z" transform="translate(-20.496 -20.201)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                    </g>
                  </svg>
                           Book An Appointment
                        </a>
              </li>
              <li>
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19">
                    <g id="online_medical_consultation-diagnosis" data-name="online medical consultation-diagnosis" transform="translate(-406.5 -115.5)">
                      <g id="_Grupo_" data-name="<Grupo>" transform="translate(407 120.645)">
                        <g id="Group_896" data-name="Group 896">
                          <path id="Path_1845" data-name="Path 1845" d="M409,142.161v-8.419A1.742,1.742,0,0,1,410.742,132h2.323" transform="translate(-408.419 -132)" fill="none" stroke="#024594" stroke-linecap="square" stroke-linejoin="round" stroke-width="1"></path>
                          <path id="Path_1846" data-name="Path 1846" d="M457.065,142.161v-8.419A1.742,1.742,0,0,0,455.323,132H453" transform="translate(-439.645 -132)" fill="none" stroke="#024594" stroke-linecap="square" stroke-linejoin="round" stroke-width="1"></path>
                          <path id="Path_1847" data-name="Path 1847" d="M422.968,173.032H409.032A2.032,2.032,0,0,1,407,171h7.258v.581h3.484V171H425A2.032,2.032,0,0,1,422.968,173.032Z" transform="translate(-407 -159.677)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                        </g>
                      </g>
                      <g id="doctor" transform="translate(409.031 116)">
                        <path id="Path_1848" data-name="Path 1848" d="M414,147.548v-2.221a2.9,2.9,0,0,1,1.667-2.628l3.267-1.538.29-1.161" transform="translate(-413.997 -133.032)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                        <path id="Path_1849" data-name="Path 1849" d="M449.226,147.548v-2.221a2.9,2.9,0,0,0-1.667-2.628l-3.267-1.538L444,140" transform="translate(-435.289 -133.032)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                        <path id="Path_1850" data-name="Path 1850" d="M431.558,123.246c1.573-.674,1.746-2.1,1.917-3.5a3.828,3.828,0,0,0-.244-2.26,2.81,2.81,0,0,0-1.615-1.3,3.7,3.7,0,0,0-2.347,0,2.81,2.81,0,0,0-1.615,1.3,3.825,3.825,0,0,0-.243,2.26c.171,1.4.344,2.821,1.917,3.5" transform="translate(-423.474 -116)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                        <path id="Path_1851" data-name="Path 1851" d="M431,144c0,1.452,2.032,2.9,2.032,2.9s2.032-1.452,2.032-2.9" transform="translate(-426.064 -135.871)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                        <g id="Group_897" data-name="Group 897" transform="translate(1.743 8.129)">
                          <path id="Path_1852" data-name="Path 1852" d="M427.407,144a2.9,2.9,0,0,0-1.452,2.9" transform="translate(-424.214 -144)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                          <path id="Path_1853" data-name="Path 1853" d="M445,144c1.452.29,1.452,2.9,1.452,2.9v1.161" transform="translate(-437.742 -144)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                          <path id="Path_1854" data-name="Path 1854" d="M420.871,157.484H420v-1.161c0-1.277.784-2.323,1.742-2.323h0c.958,0,1.742,1.045,1.742,2.323v1.161h-.871" transform="translate(-420 -151.097)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></path>
                          <circle id="Ellipse_14" data-name="Ellipse 14" cx="1" cy="1" r="1" transform="translate(7.226 3.871)" fill="none" stroke="#024594" stroke-linecap="round" stroke-linejoin="round" stroke-width="1"></circle>
                        </g>
                      </g>
                    </g>
                  </svg>
                           Online Services
                        </a>
              </li>
              <li>
                <a href="#">
                  <svg xmlns="http://www.w3.org/2000/svg" width="14.344" height="17" viewBox="0 0 14.344 17">
                    <g id="Group_902" data-name="Group 902" transform="translate(-21 -16)">
                      <g id="Group_901" data-name="Group 901" transform="translate(21 16)">
                        <path id="Path_1866" data-name="Path 1866" d="M28.57,25.031a4.516,4.516,0,1,0-.8,0A7.272,7.272,0,0,0,21,32.469V33H35.344v-.531A7.272,7.272,0,0,0,28.57,25.031Zm-3.852-4.516a3.453,3.453,0,1,1,3.453,3.453A3.446,3.446,0,0,1,24.719,20.516Zm-2.63,11.422a6.088,6.088,0,0,1,12.166,0Z" transform="translate(-21 -16)" fill="#024594"></path>
                      </g>
                    </g>
                  </svg>
                           Register/Login
                        </a>
              </li>
            </ul>
          </div></nav>

      </div>
    </div>
    )
}

export default TopBar
