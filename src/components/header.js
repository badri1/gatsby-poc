import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import TopBar from "./topbar"
import MidBar from "./midbar"
import HeaderMenu from "./headerMenu"

const Header = ({ siteTitle }) => (
  <header>
    <TopBar />
    <nav class="navbar navbar-expand-lg flex-wrap p-0">
      <MidBar />
      <div class="break"></div>
      <HeaderMenu />
    </nav>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
