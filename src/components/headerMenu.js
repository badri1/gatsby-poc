import React from "react"

function HeaderMenu(props) {
  return (
    <div class="collapse navbar-collapse
               navbarSupportedContent main-nav">
      <div class="container">
        <ul class="navbar-nav">
          <li class="nav-item active dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Patient Care
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul class="row">
                <li class="col-md-4"><a href="#"> Find a Doctor</a></li>
                <li class="col-md-4"><a href="#">Request an appointment</a></li>
                <li class="col-md-4"><a href="#">Out-Patient (OPD)</a></li>
                <li class="col-md-4"><a href="#">In-Patient</a></li>
                <li class="col-md-4"><a href="#">View Reports</a></li>
                <li class="col-md-4"><a href="#">Make Payment</a></li>
                <li class="col-md-4"><a href="#">View Bills</a></li>
                <li class="col-md-4"><a href="#">Care@Home</a></li>
                <li class="col-md-4"><a href="#">Preventive Healthcare</a></li>
                <li class="col-md-4"><a href="#">Insurance &amp; TPA</a></li>
                <li class="col-md-4"><a href="#">Short Stay Service</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Doctors
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> Find a Doctor</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Specialities &amp;
              Clinics
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Research &amp;
              Academics
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Philanthrophy
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              About Us
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown show_on_mobile">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Book an Appointment
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown show_on_mobile">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Online Services
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item dropdown show_on_mobile">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Register / Login
                        </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <ul>
                <li><a href="#"> sub link 1</a></li>
                <li><a href="#"> sub link 2</a></li>
              </ul>
            </div>
          </li>
          <li class="nav-item show_on_mobile download_app_nav">
            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img class="lazyload img-responsive" data-src="assets/images/mob-menu-download-app.gif" alt=""/>
                        </a>
                     </li>
            <li class="nav-item dropdown show_on_mobile">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Contact us
                        </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <ul>
                  <li><a href="#"> sub link 1</a></li>
                  <li><a href="#"> sub link 2</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item dropdown show_on_mobile">
              <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Emergency Helpline
                        </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <ul>
                  <li><a href="#"> sub link 1</a></li>
                  <li><a href="#"> sub link 2</a></li>
                </ul>
              </div>
            </li>
          </ul>
          <div class="show_on_mobile social_links_contact">
            <div class="emergency_contact_mob">
              <p>Emergency Contact</p>
              <p class="contact_no"><a href="tel:022 2445 2575">022 2445 2575</a> / <a href="tel:+91 98 2088 5000">+91 98 2088 5000</a></p>
            </div>
            <div class="social_icons">
              <ul class="list-unstyled follow_links">
                <li>
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16.924" height="18.849" viewBox="0 0 16.924 18.849">
                      <path id="Icon_zocial-linkedin" data-name="Icon zocial-linkedin" d="M-.09,2.3A2.224,2.224,0,0,1,.482.727,1.931,1.931,0,0,1,1.968.108,1.875,1.875,0,0,1,3.422.717a2.339,2.339,0,0,1,.572,1.637,2.175,2.175,0,0,1-.555,1.523,1.936,1.936,0,0,1-1.5.628H1.919A1.848,1.848,0,0,1,.465,3.878,2.3,2.3,0,0,1-.09,2.3ZM.122,18.957V6.239H3.749V18.957H.122Zm5.636,0H9.385v-7.1a3.157,3.157,0,0,1,.131-1.028,2.774,2.774,0,0,1,.694-1.095,1.618,1.618,0,0,1,1.168-.447q1.83,0,1.83,2.875v6.8h3.627V11.665a6.818,6.818,0,0,0-1.144-4.274,3.718,3.718,0,0,0-6.306.657v.038H9.369l.016-.038V6.239H5.758q.033.609.033,3.789T5.758,18.957Z" transform="translate(0.09 -0.108)" fill="#024594"></path>
                    </svg>
                  </a>
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.408" height="19.97" viewBox="0 0 25.408 19.97">
                      <path id="Icon_simple-youtube" data-name="Icon simple-youtube" d="M24.881,5.8A3.407,3.407,0,0,0,22.67,3.309c-1.98-.6-9.951-.6-9.951-.6s-7.95-.012-9.951.6A3.407,3.407,0,0,0,.557,5.8,41.727,41.727,0,0,0,0,12.709,41.726,41.726,0,0,0,.557,19.6a3.407,3.407,0,0,0,2.211,2.487c1.978.6,9.951.6,9.951.6s7.949,0,9.951-.6A3.407,3.407,0,0,0,24.881,19.6a41.735,41.735,0,0,0,.53-6.888,41.735,41.735,0,0,0-.53-6.914ZM10.175,16.986V8.419l6.634,4.29Z" transform="translate(-0.004 -2.712)" fill="#024594"></path>
                    </svg>
                  </a>
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.408" height="20.657" viewBox="0 0 25.408 20.657">
                      <path id="Icon_zocial-twitter" data-name="Icon zocial-twitter" d="M-2.369,18.143q.62.06,1.239.061a10.247,10.247,0,0,0,6.486-2.23,5.023,5.023,0,0,1-3.036-1.034A5.08,5.08,0,0,1,.482,12.358a4.912,4.912,0,0,0,.971.083,5.106,5.106,0,0,0,1.384-.186A5.026,5.026,0,0,1-.169,10.469,5.051,5.051,0,0,1-1.357,7.153V7.09a4.89,4.89,0,0,0,2.375.64,5.117,5.117,0,0,1-1.7-1.849,5.082,5.082,0,0,1-.63-2.488A5.063,5.063,0,0,1-.592.77,14.493,14.493,0,0,0,4.149,4.623a14.673,14.673,0,0,0,6,1.6A5.861,5.861,0,0,1,10,5.026a5.013,5.013,0,0,1,1.529-3.677,5.038,5.038,0,0,1,3.7-1.529,5.008,5.008,0,0,1,3.8,1.653A10.488,10.488,0,0,0,22.337.193a4.968,4.968,0,0,1-2.272,2.892,10.643,10.643,0,0,0,2.975-.826,10,10,0,0,1-2.583,2.706v.683a14.948,14.948,0,0,1-.609,4.183,14.884,14.884,0,0,1-1.859,4.008A15.959,15.959,0,0,1,15,17.235,13.231,13.231,0,0,1,10.841,19.6a15.212,15.212,0,0,1-5.216.877A14.635,14.635,0,0,1-2.369,18.143Z" transform="translate(2.369 0.18)" fill="#024594"></path>
                    </svg>
                  </a>
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12.704" height="23.613" viewBox="0 0 12.704 23.613">
                      <path id="Icon_zocial-facebook" data-name="Icon zocial-facebook" d="M6.629,12.68V8.171h3.637V5.9a5.934,5.934,0,0,1,1.593-4.18A5.068,5.068,0,0,1,15.72,0h3.613V4.51H15.72a.8.8,0,0,0-.637.39,1.582,1.582,0,0,0-.283.956V8.17h4.533V12.68H14.8V23.613H10.265V12.68Z" transform="translate(-6.629)" fill="#024594"></path>
                    </svg>
                  </a>
                  <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" width="23.613" height="23.613" viewBox="0 0 23.613 23.613">
                      <path id="Icon_zocial-instagram" data-name="Icon zocial-instagram" d="M2.118,23.6a2.807,2.807,0,0,1-1.292-.734,2.6,2.6,0,0,1-.715-1.232C.028,21.307.024,21,.024,11.885.019,1.647.005,2.21.273,1.633A2.754,2.754,0,0,1,1.615.3c.6-.263.014-.249,10.33-.235l9.5.009.3.1a2.8,2.8,0,0,1,1.772,1.772l.1.3.009,9.5c.014,10.556.037,9.767-.3,10.45a2.741,2.741,0,0,1-1.167,1.167c-.674.332.092.309-10.353.3C3.821,23.664,2.354,23.655,2.118,23.6Zm18.391-2.81c.378-.268.388-.3.4-2.053.014-1.712,0-1.8-.277-2.072-.254-.249-.374-.263-2.044-.258l-1.523,0-.208.111a1.134,1.134,0,0,0-.341.323l-.138.2,0,1.527c0,.835.014,1.6.037,1.684a.862.862,0,0,0,.471.581l.231.115,1.6-.014,1.6-.014Zm-7.76-4.48a4.579,4.579,0,0,0,3.534-3.553,5.636,5.636,0,0,0,.051-1.5,4.647,4.647,0,0,0-2.367-3.4,4.951,4.951,0,0,0-1.218-.452c-.088-.009-.272-.037-.411-.06A4.558,4.558,0,0,0,8.222,9.112a4.514,4.514,0,0,0,.088,5.606,4.6,4.6,0,0,0,3.045,1.656A4.815,4.815,0,0,0,12.749,16.31ZM4.716,13.315a7.322,7.322,0,0,1,.609-4.683,6.787,6.787,0,0,1,1.38-1.9,7.2,7.2,0,0,1,6.279-2.049,7.309,7.309,0,0,1,5.878,5.352,7.165,7.165,0,0,1,.189,2.584,5.08,5.08,0,0,1-.189,1c-.014.023-.009.042.014.046.175.018,1.97.014,2-.009s.037-2.062.032-5.149L20.9,3.382l-.138-.2a1.153,1.153,0,0,0-.346-.3L20.2,2.773l-8.425.014L3.359,2.8,3.17,2.92a1.214,1.214,0,0,0-.3.3L2.764,3.4l-.014,5.1c0,2.805,0,5.117.009,5.144a4.965,4.965,0,0,0,1.029.046H4.8Z" transform="translate(-0.022 -0.058)" fill="#024594"></path>
                    </svg>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="btn_search show_on_desktop">
            <button>
              <svg xmlns="http://www.w3.org/2000/svg" width="32.959" height="32.959" viewBox="0 0 32.959 32.959">
                <g id="" transform="translate(-1153.5 -138.5)">
                  <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(1155 140)">
                    <path id="Path_252" data-name="Path 252" d="M30.564,17.532A13.032,13.032,0,1,1,17.532,4.5,13.032,13.032,0,0,1,30.564,17.532Z" transform="translate(-4.5 -4.5)" fill="none" stroke="#fff" stroke-linejoin="round" stroke-width="3"></path>
                    <path id="Path_253" data-name="Path 253" d="M33.137,33.137l-8.162-8.162" transform="translate(-2.739 -2.739)" fill="none" stroke="#fff" stroke-linejoin="round" stroke-width="3"></path>
                  </g>
                </g>
              </svg>
            </button>
          </div>
               </div>
      </div>
    )
}
export default HeaderMenu
