import React from "react"
import logo from "./icons/Logo.svg"

function MidBar(props) {
  return (
    <div class="container">
      <a class="navbar-brand" href="#" aria-label="Logo"><img class=" ls-is-cached lazyloaded" src={logo} alt="Hinduja hospitals"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
            <i></i>
            <i></i>
            <i></i>
          </span>
        </button>
        <div class="btn_search show_on_mobile">
          <button title="Search" type="button" aria-label="Search button">
            <svg xmlns="http://www.w3.org/2000/svg" width="32.959" height="32.959" viewBox="0 0 32.959 32.959">
              <g id="" transform="translate(-1153.5 -138.5)">
                <g id="Icon_feather-search" data-name="Icon feather-search" transform="translate(1155 140)">
                  <path id="Path_252" data-name="Path 252" d="M30.564,17.532A13.032,13.032,0,1,1,17.532,4.5,13.032,13.032,0,0,1,30.564,17.532Z" transform="translate(-4.5 -4.5)" fill="none" stroke="#fff" stroke-linejoin="round" stroke-width="3"></path>
                  <path id="Path_253" data-name="Path 253" d="M33.137,33.137l-8.162-8.162" transform="translate(-2.739 -2.739)" fill="none" stroke="#fff" stroke-linejoin="round" stroke-width="3"></path>
                </g>
              </g>
            </svg>
          </button>
        </div>
        <ul class="navbar-nav ml-auto contact_nav">
          <li class="nav-item callus-item">
            <a class="nav-link" href="#">
              <img class=" lazyloaded" data-src="assets/images/icons/call_us.svg" alt="" src="assets/images/icons/call_us.svg"/>
                Call us
            </a>
            <ul class="call_links">
              <li>24/7 Board :
                          <a href="tel:02224452222">022 24452222</a> / <a href="tel:02224451515">022 24451515</a>
              </li>
              <li>For OPD appointment : <a href="#"> 022 67668181</a> / <a href="#">022 45108181</a>

              </li>
              <li>Emergency :  <a href="tel:02224452575">022 24452575</a>  / <a href="tel:09820885000">09820885000</a></li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">
              <img class=" lazyloaded" data-src="assets/images/icons/download_app.svg" alt="" src="assets/images/icons/download_app.svg"/>
                Download <br/>Mobile
                    App</a>
          </li>
        </ul>
      </div>
    )
}

export default MidBar
