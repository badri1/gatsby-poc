import React from "react"

import Webform from 'gatsby-drupal-webform'

import { navigate } from 'gatsby'

import Layout from "../components/layout"
import SEO from "../components/seo"

export default function ContactForm({ data }) {
  return (
  <Layout>
    <SEO title="Page two" />
  	<Webform
	  	webform={data.webformWebform}
		  endpoint="http://gatsby-pf.lndo.site/react_webform_backend/submit"
		  onSuccess={(response) => navigate(response.settings.confirmation_url)}
	  />
  </Layout>
)
  }

export const query = graphql`
	query {
		webformWebform(drupal_internal__id: { eq: "contact" }) {
			drupal_internal__id
			elements {
				name
				type
				attributes {
					name
					value
				}
			}
		}
	}
  `
