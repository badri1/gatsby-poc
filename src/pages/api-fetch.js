import React, { useState, useEffect } from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"


const ApiFetch = () => {
  // Client-side Runtime Data Fetching
  const [starsCount, setStarsCount] = useState(0)
  useEffect(() => {
    // get data from GitHub api
    fetch(`https://api.github.com/repos/gatsbyjs/gatsby`)
      .then(response => response.json()) // parse JSON from request
      .then(resultData => {
        setStarsCount(resultData.stargazers_count)
      }) // set data for the number of stars
  }, [])
  return (
  <Layout>
    <SEO title="API Fetch" />
    <p>Nascetur metus natoque, wisi, repellendus sapien mattis, egestas tempor eligendi incididunt repudiandae, neque mattis ligula, nibh eum eiusmod. Senectus nihil cum ipsum, porttitor rutrum tempore reprehenderit, dictum, mus commodo. Excepteur.</p>
    <p>Corporis temporibus. Tempus, do suspendisse cursus nunc quaerat vulputate! Proident placeat wisi? Feugiat cursus integer! Quam? Praesent veniam, labore deserunt? Natoque, expedita? Numquam? Reiciendis? Posuere minima tenetur id metus porta.</p>
    <p>Nisi ratione doloremque ullam dolorum, iure! Accumsan nibh aenean soluta risus accusantium, dicta pretium fuga blandit eros, cupiditate, dignissimos deserunt? Reprehenderit porro ante eum, autem! Quasi gravida enim laborum amet.</p>
    <section>
      <p>Runtime Data: Star count for the Gatsby repo {starsCount}</p>
    </section>
    </Layout>
  )
}
export default ApiFetch
