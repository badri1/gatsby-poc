/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

import "./src/css/bootstrap.min.css"
import "./src/css/common.css"
import "./src/css/about.css"
